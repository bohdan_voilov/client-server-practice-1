import lombok.AllArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class StoreService {
    private final Connection connection;

    public int createItem(String title, int price) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO items(title,price) VALUES (?,?) RETURNING id");
        preparedStatement.setString(1, title);
        preparedStatement.setInt(2, price);

        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getInt("id");
    }

    public void updateItem(Item item) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("UPDATE items SET title=?,price=? WHERE id=?;");
        preparedStatement.setString(1, item.getTitle());
        preparedStatement.setInt(2, item.getPrice());

        preparedStatement.setInt(3, item.getId());

        preparedStatement.executeUpdate();
    }

    public List<Item> readItems() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM items;");
        ResultSet resultSet = preparedStatement.executeQuery();

        return readResultSet(resultSet);
    }

    public void deleteItem(int id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM items WHERE id=?;");
        preparedStatement.setInt(1, id);

        preparedStatement.execute();
    }

    public void clean() throws SQLException {
        connection.prepareStatement("TRUNCATE TABLE items;").execute();
        connection.prepareStatement("ALTER SEQUENCE items_id_seq RESTART WITH 1").execute();
    }

    private List<Item> readResultSet(ResultSet resultSet) throws SQLException {
        List<Item> items = new ArrayList<>();

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String title = resultSet.getString("title");
            int price = resultSet.getInt("price");
            items.add(new Item(id, title, price));
        }
        return items;
    }


    public List<Item> selectWith(Map<String,Object> filterMap) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM items WHERE ?? = ?;");
        var i = 1;
        for(var key : filterMap.keySet()){
            var value = filterMap.get(key);
            switch (value.getClass().getSimpleName()){
                case "Integer":
                    preparedStatement.setString(i++,key);
                    preparedStatement.setInt(i++, (Integer)value);
                    break;
                case "String":
                    preparedStatement.setString(i++,key);
                    preparedStatement.setString(i++, (String)value);
                    break;

            }

        }
        System.out.println(preparedStatement.toString());
        return readResultSet(preparedStatement.executeQuery());
    }

}
