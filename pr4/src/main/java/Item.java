import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Item {
    private int id;
    private String title;
    private int price;
}
