import lombok.SneakyThrows;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;

public class StoreServiceTest {
    static StoreService storeService;
    @SneakyThrows
    @BeforeAll()
    public static void init(){
        Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/store", "postgres", "secret");
        storeService = new StoreService(conn);
    }

    @SneakyThrows
    @BeforeEach()
    public void clean(){
        storeService.clean();
    }

    @SneakyThrows
    @Test
    public void testCreate(){
        Assertions.assertEquals(1,storeService.createItem("Item 1",200));
        Assertions.assertEquals(2,storeService.createItem("Item 2",300));
        List<Item> items = Arrays.asList(new Item(1,"Item 1",200),new Item(2,"Item 2",300));
        Assertions.assertTrue(storeService.readItems().equals(items));
    }


    @SneakyThrows
    @Test
    public void testUpdate(){
        storeService.createItem("Item 1",200);
                storeService.updateItem(new Item(1,"Updated",300));
        List<Item> items = Arrays.asList(new Item(1,"Updated",300));
        Assertions.assertTrue(storeService.readItems().equals(items));
    }

    @SneakyThrows
    @Test
    public void testDelete(){
        storeService.createItem("Item 1",200);
        storeService.deleteItem(1);
        List<Item> items = new ArrayList<>();
        Assertions.assertTrue(storeService.readItems().equals(items));
    }

    @SneakyThrows
    @Test
    public void testSelectWithCriteria(){
        Map<String,Object> criterias = new HashMap();
        criterias.put("title", "Item 1");
        storeService.createItem("Item 1",200);
        storeService.createItem("Item 2",300);
        storeService.createItem("Item 3",400);
        List<Item> items = Arrays.asList(new Item(1,"Item 1",200));
        System.out.println(storeService.selectWith(criterias));
        Assertions.assertTrue(storeService.selectWith(criterias).equals(items));
    }

}
