package protocol;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class ProtocolPacketWriter {
    public byte[] write(ProtocolPacket packet) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance(ProtocolPacket.ENCRYPTION_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE,ProtocolPacket.ENCRYPTION_KEY);
        byte[] message = cipher.doFinal(new ProtocolMessageWriter().write(packet.getMessage()));

        ByteBuffer buffer = ByteBuffer.allocate(1+1+8+4+2+message.length+2);
        buffer.put(ProtocolPacket.MAGIC_BYTE);
        buffer.put(packet.getClientId());
        buffer.putLong(packet.getPacketId());
        buffer.putInt(message.length);

        byte[] head = new byte[14];
        buffer.position(0);
        buffer.get(head,0,14);
        buffer.position(14);
        buffer.put(ProtocolPacket.crc16(head));

        buffer.put(message);

        buffer.put(ProtocolPacket.crc16(message));
        return buffer.array();
    }
}
