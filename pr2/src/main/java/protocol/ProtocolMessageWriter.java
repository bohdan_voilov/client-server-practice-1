package protocol;

import java.nio.ByteBuffer;

public class ProtocolMessageWriter {
    public byte[] write(ProtocolMessage message){
        ByteBuffer buffer = ByteBuffer.allocate(4+4+message.getData().length);
        buffer.putInt(message.getCommandCode());
        buffer.putInt(message.getUserId());
        buffer.put(message.getData());
        return buffer.array();
    }
}
