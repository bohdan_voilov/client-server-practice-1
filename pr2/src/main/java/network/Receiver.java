package network;

import io.reactivex.rxjava3.core.Observable;
import protocol.ProtocolPacket;


public interface Receiver {
    Observable<ProtocolPacket> protocolPacketStream();
}
