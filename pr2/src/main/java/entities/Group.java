package entities;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Group {
    private String name;
    private int count;
}
