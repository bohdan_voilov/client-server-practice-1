import lombok.SneakyThrows;
import tcp.StoreTcpClient;


public class TCPClient {
    @SneakyThrows
    public static void main(String[] args) {
        var t1 = new Thread(()->{
            StoreTcpClient client = new StoreTcpClient("127.0.0.1",8080);
            try {
                System.out.println(client.trySendMessage("Teest"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        var t2 = new Thread(()->{
            StoreTcpClient client = new StoreTcpClient("127.0.0.1",8080);
            try {
                System.out.println(client.trySendMessage("Teest"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        var t3 = new Thread(()->{
            StoreTcpClient client = new StoreTcpClient("127.0.0.1",8080);
            try {
                System.out.println(client.trySendMessage("Teest"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();
        t3.start();
        t3.join();

    }
}
