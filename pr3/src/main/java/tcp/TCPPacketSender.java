package tcp;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import network.Handled;
import network.PacketSender;
import network.Serializer;
import protocol.ProtocolMessage;
import protocol.ProtocolPacket;
import protocol.ProtocolPacketWriter;

import java.io.OutputStream;
import java.net.Socket;

@AllArgsConstructor
public class TCPPacketSender implements PacketSender {
    private final OutputStream outputStream;
    @SneakyThrows
    @Override
    public void send(Handled handled) {
        byte[] res = new ProtocolPacketWriter().write(new ProtocolPacket(handled.getClientId(),handled.getPacketId(),new ProtocolMessage(0,0, Serializer.serialize(handled.getRes()))));
        outputStream.write(res);
    }

    @SneakyThrows
    public void send(byte clientId, long packetId, int command, int userId, Object payload){
        byte[] res = new ProtocolPacketWriter().write(new ProtocolPacket(clientId,packetId,new ProtocolMessage(command,userId, Serializer.serialize(payload))));
        outputStream.write(res);
        outputStream.flush();
    }
}
