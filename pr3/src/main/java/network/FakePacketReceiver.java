package network;

import io.reactivex.rxjava3.core.Observable;
import protocol.ProtocolMessage;
import protocol.ProtocolPacket;
import services.AddItemPriceRequest;
import services.ChangeItemCountRequest;
import services.GetItemCountRequest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FakePacketReceiver{

    private ProtocolPacket getItemCountRequest(String groupName, String itemName, long reqCount) throws IOException {
        return new ProtocolPacket((byte)0,reqCount,new ProtocolMessage(1,1,Serializer.serialize(new GetItemCountRequest(groupName,itemName))));
    }

    private ProtocolPacket removeItemCountRequest(String groupName, String itemName, int count, long reqCount) throws IOException {
        return new ProtocolPacket((byte)0,reqCount,new ProtocolMessage(2,1,Serializer.serialize(new ChangeItemCountRequest(groupName,itemName,-count))));
    }

    private ProtocolPacket addItemCountRequest(String groupName, String itemName, int count, long reqCount) throws IOException {
        return new ProtocolPacket((byte)0,reqCount,new ProtocolMessage(3,1,Serializer.serialize(new ChangeItemCountRequest(groupName,itemName,count))));
    }

    private ProtocolPacket addGroupRequest(String groupName, long reqCount) throws IOException {
        return new ProtocolPacket((byte)0,reqCount,new ProtocolMessage(4,1,Serializer.serialize(groupName)));
    }

    private ProtocolPacket addGroupItemRequest(String groupName, String itemName, int price, long reqCount) throws IOException {
        return new ProtocolPacket((byte)0,reqCount,new ProtocolMessage(5,1,Serializer.serialize(new AddItemPriceRequest(groupName,itemName,price))));
    }

    private ProtocolPacket setGroupItemPriceRequest(String groupName, String itemName, int price, long reqCount) throws IOException {
        return new ProtocolPacket((byte)0,reqCount,new ProtocolMessage(6,1,Serializer.serialize(new AddItemPriceRequest(groupName,itemName,price))));
    }

    public Observable<ProtocolPacket> protocolPacketStream() {

        Stream <ProtocolPacket> packetStream = IntStream.range(0,1000).mapToObj(i-> {
            try {
                byte[] value =Serializer.serialize("hello world");
                return new ProtocolPacket((byte)0,(long)i,new ProtocolMessage(1,1,value));
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        });
        try{
            Observable<ProtocolPacket> protocolPacketStream = Observable.just(
                    addGroupRequest("group1",1),
                    addGroupItemRequest("group1","item1",310,2),
                    addGroupItemRequest("group1","item2",330,3),
                    addItemCountRequest("group1","item1",100,4),
                    addItemCountRequest("group1","item1",100,5),
                    addItemCountRequest("group1","item1",100,8),
                    getItemCountRequest("group1","item1",9),
                   removeItemCountRequest("group1","item1",100,10),
                    setGroupItemPriceRequest("group1","item1",400,11)

            ).delay(1, TimeUnit.SECONDS);
            return protocolPacketStream;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
