package network;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Handled<T> {
    private final T res;
    private final byte clientId;
    private final long packetId;
}
