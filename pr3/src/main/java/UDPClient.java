import lombok.SneakyThrows;
import udp.StoreUDPClient;

public class UDPClient {
    @SneakyThrows
    public static void main(String[] args) {
        var t1 = new Thread(()->{
            var client = new StoreUDPClient((byte)1,8080);
            System.out.println(client.trySendMessage("test"));
        });

        var t2 = new Thread(()->{
            var client = new StoreUDPClient((byte)2,8080,true);
            System.out.println(client.trySendMessage("test"));
            System.out.println(client.trySendMessage("test2"));

        });

        var t3 = new Thread(()->{
            var client = new StoreUDPClient((byte)3,8080);
            System.out.println(client.trySendMessage("test"));
        });

        t1.start();
        t2.start();
        t3.start();
        t3.join();

    }
}
