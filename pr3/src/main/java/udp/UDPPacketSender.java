package udp;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import network.Handled;
import network.PacketSender;
import network.Serializer;
import protocol.ProtocolMessage;
import protocol.ProtocolPacket;
import protocol.ProtocolPacketWriter;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

@AllArgsConstructor
public class UDPPacketSender implements PacketSender {
    private final DatagramSocket socket;
    private final InetAddress address;
    private final int port;
    @SneakyThrows
    @Override
    public void send(Handled handled) {
        byte[] res = new ProtocolPacketWriter().write(new ProtocolPacket(handled.getClientId(),handled.getPacketId(),new ProtocolMessage(0,0, Serializer.serialize(handled.getRes()))));
        DatagramPacket packet = new DatagramPacket(res,res.length,address,port);
        socket.send(packet);
    }

    @SneakyThrows
    public void send(byte clientId, long packetId, int command, int userId, Object payload){
        byte[] res = new ProtocolPacketWriter().write(new ProtocolPacket(clientId,packetId,new ProtocolMessage(command,userId, Serializer.serialize(payload))));
        DatagramPacket packet = new DatagramPacket(res,res.length,address,port);
        socket.send(packet);
    }
}
