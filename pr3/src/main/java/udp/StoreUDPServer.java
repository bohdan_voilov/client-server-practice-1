package udp;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import network.Handled;
import network.PacketReceiver;
import network.PacketSender;
import network.Serializer;
import protocol.ProtocolPacket;
import protocol.ProtocolServer;
import tcp.StoreTCPServer;
import tcp.TCPPacketReceiver;
import tcp.TCPPacketSender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class StoreUDPServer extends ProtocolServer {
    private final Map<Integer, Function> handlers = new HashMap<>();
    @AllArgsConstructor
    private class ServerThread extends Thread {
        private final PacketReceiver packetReceiver;
        private final PacketSender packetSender;

        @SneakyThrows
        @Override
        public void run() {
            System.out.println("Server run");
            ProtocolPacket packet;
            try {
                packet = packetReceiver.protocolPacket();
            } catch (Exception e){
                e.printStackTrace();
                return;
            }
            var alreadyProcessed = alreadyProcessed(packet.getClientId(),packet.getPacketId());
            if(alreadyProcessed){
                packetSender.send(new Handled("Already processed",packet.getClientId(),packet.getPacketId()));
                return;
            }
            Object req = Serializer.deserialize(packet.getMessage().getData());
            Object res = handlers.get(packet.getMessage().getCommandCode()).apply(req);
            Handled h = new Handled(res, packet.getClientId(), packet.getPacketId());
            packetSender.send(h);
        }
    }
    public <Req, Res> void handleCommand(int commandCode, Function<Req,Res> handler){
        handlers.put(commandCode,handler);
    }



    @SneakyThrows
    public void run(int port) throws IOException {
        DatagramSocket serverSocket = new DatagramSocket(port);
        System.out.println("UDP server started at "+port);
        int counter = 0;
        while(true){
            try {
                counter++;

                byte[] buff = new byte[65535];
                DatagramPacket packet = new DatagramPacket(buff,buff.length);
                serverSocket.receive(packet);
                System.out.println("counter "+counter);
                if(counter%2==0){
                    System.out.println("Skipping");
                    continue;
                }
                System.out.println("Accepted");
                System.out.println(packet.getLength());
                new StoreUDPServer.ServerThread(new UDPPacketReceiver(packet),new UDPPacketSender(serverSocket,packet.getAddress(),packet.getPort())).start();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
