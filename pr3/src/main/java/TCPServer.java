import entities.Store;
import lombok.SneakyThrows;
import services.AddItemPriceRequest;
import services.ChangeItemCountRequest;
import services.GetItemCountRequest;
import tcp.StoreTCPServer;

public class TCPServer {

    @SneakyThrows
    public static void main(String[] args) {
        StoreTCPServer tcpServer=new StoreTCPServer();
        Store store = new Store();
        tcpServer.handleCommand(1, (GetItemCountRequest r) -> {
            synchronized (store) {
                int res = store.itemCount(r.getGroupName(), r.getItemName());
                return res;
            }
        });

        tcpServer.handleCommand(2, (ChangeItemCountRequest r) -> {
            synchronized (store) {
                store.addItemCount(r.getGroupName(), r.getItemName(), r.getItemCount());
            }
            return "Ok";
        });

        tcpServer.handleCommand(3, (ChangeItemCountRequest r) -> {
            synchronized (store) {
                store.addItemCount(r.getGroupName(), r.getItemName(), r.getItemCount());
            }
            return "Ok";
        });

        tcpServer.handleCommand(4, (String s) -> {
            synchronized (store) {
                store.addGroup(s);
            }
            System.out.println("COMMAND 4 "+s);
            return "Ok";
        });

        tcpServer.handleCommand(5, (AddItemPriceRequest r) -> {
            synchronized (store) {
                store.addItem(r.getGroupName(), r.getItemName(), r.getPrice());
            }
            return "Ok";
        });

        tcpServer.handleCommand(6, (AddItemPriceRequest r) -> {
            synchronized (store) {
                store.setItemPrice(r.getGroupName(), r.getItemName(), r.getPrice());
            }
            return "Ok";
        });

        tcpServer.run(8080);
    }
}
