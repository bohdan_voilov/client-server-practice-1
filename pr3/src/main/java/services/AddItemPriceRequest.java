package services;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor @Getter
public class AddItemPriceRequest implements Serializable {
    private final String groupName;
    private final String itemName;
    private final int price;
}
