package protocol;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.InputMismatchException;


public class ProtocolPacketReaderTest {
    private ProtocolPacket packet = new ProtocolPacket((byte)0,1l,new ProtocolMessage(1,1,"Hello World".getBytes()));
    private byte[] validPacketBytes = Base64.getDecoder().decode("EwAAAAAAAAAAAQAAACAuy+SQ0XBCSNQpKKsulhOdZj0Ot0J4kjbjHzLwhwemHj5Bym0=");

    @Test()
    public void invalidMagicByte(){
        byte[] input = new byte[14];
        ProtocolPacketReader reader = new ProtocolPacketReader(input);
        Exception exception = Assertions.assertThrows(InputMismatchException.class,()->{
            reader.readPacket();
        });
        Assertions.assertEquals("Magic byte not found",exception.getMessage());
    }

    @Test()
    public void invalidHeaderCRC(){
        byte[] input = new byte[18];
        input[0] = ProtocolPacket.MAGIC_BYTE;
        ProtocolPacketReader reader = new ProtocolPacketReader(input);
        Exception exception = Assertions.assertThrows(InputMismatchException.class,()->{
            reader.readPacket();
        });
        Assertions.assertEquals("Head crc does not match",exception.getMessage());
    }

    @Test()
    public void invalidMessageCRC(){
        byte[] input = Arrays.copyOf(validPacketBytes,validPacketBytes.length);
        input[17] = (byte) (input[17] ^ 1);
        ProtocolPacketReader reader = new ProtocolPacketReader(input);
        Exception exception = Assertions.assertThrows(InputMismatchException.class,()->{
            reader.readPacket();
        });
        Assertions.assertEquals("Message crc does not match",exception.getMessage());
    }

    @Test()
    public void correctMessage() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        byte[] input = Arrays.copyOf(validPacketBytes,validPacketBytes.length);
        ProtocolPacketReader reader = new ProtocolPacketReader(input);
        ProtocolPacket packet =
            reader.readPacket();
        Assertions.assertEquals(this.packet.getPacketId(),packet.getPacketId());
        Assertions.assertEquals(this.packet.getClientId(),packet.getClientId());
        Assertions.assertEquals(this.packet.getMessage().getUserId(),packet.getMessage().getUserId());
        Assertions.assertEquals(this.packet.getMessage().getCommandCode(),packet.getMessage().getCommandCode());
        Assertions.assertArrayEquals(this.packet.getMessage().getData(),packet.getMessage().getData());
    }
}
