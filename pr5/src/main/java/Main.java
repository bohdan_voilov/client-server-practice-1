import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Store;
import handlers.*;
import handlers.core.Handler;
import handlers.core.Router;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Main {
    private static ObjectMapper objectMapper = new ObjectMapper();
    private static Store store = new Store();
    public static void main(String[] args) throws IOException {
        Router router = new Router(new InetSocketAddress(8080),1);
        router.addRoute(new Handler("/login","POST",new LoginHandler(objectMapper)));
        router.addRoute(new Handler("/api/good/(\\d+)","GET",new GetItemHandler(store,objectMapper)));
        router.addRoute(new Handler("/api/good/(\\d+)","POST",new UpdateItemHandler(store,objectMapper)));
        router.addRoute(new Handler("/api/good/(\\d+)","DELETE",new DeleteItemHandler(store)));
        router.addRoute(new Handler("/api/good","PUT",new CreateItemHandler(store,objectMapper)));

        router.start(new Auth());
    }


}
