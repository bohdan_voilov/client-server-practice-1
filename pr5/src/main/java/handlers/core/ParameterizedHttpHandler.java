package handlers.core;

import com.sun.net.httpserver.HttpHandler;

import java.util.ArrayList;
import java.util.List;

public abstract class ParameterizedHttpHandler implements HttpHandler {
    protected List<String> params = new ArrayList();
    public void addPathParam( String value){
        this.params.add(value);
    }

}
