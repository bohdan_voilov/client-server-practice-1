package handlers;

import com.sun.net.httpserver.HttpExchange;
import entity.Store;
import handlers.core.ParameterizedHttpHandler;
import lombok.AllArgsConstructor;

import java.io.IOException;

@AllArgsConstructor
public class DeleteItemHandler extends ParameterizedHttpHandler {
    private final Store store;
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        int id = Integer.valueOf(this.params.get(0));
        boolean existed = store.delete(id);
        if(existed){
            exchange.sendResponseHeaders(204,0);
            exchange.getResponseBody().close();
            return;
        }
        exchange.sendResponseHeaders(404,0);
        exchange.getResponseBody().close();
        return;
    }
}
