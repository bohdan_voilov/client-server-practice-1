package handlers;


import com.sun.net.httpserver.Authenticator;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpPrincipal;
import util.Token;

public class Auth extends Authenticator {
    @Override
    public Result authenticate(HttpExchange httpExchange) {
        if (!httpExchange.getRequestURI().toString().startsWith("/api/")){
            return new Success(null);
        }
        try {
            String token = httpExchange.getRequestHeaders().get("x-auth-token").get(0);
            Token.check(token);
        }catch (Exception e){
            e.printStackTrace();
            return new Failure(403);
        }
        return new Success(new HttpPrincipal("", "realm"));

    }
}
