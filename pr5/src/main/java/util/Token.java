package util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.HashMap;

public class Token {
    private final static Algorithm algorithm =  Algorithm.HMAC256("secret");
    public static String token(String login){
        String token = JWT.create()
                .withIssuer("auth0")
                .withPayload(new HashMap<>(){{
                    put("login",login);
                }})
                .sign(algorithm);
        return token;
    }

    public static String check(String token){
        DecodedJWT jwt = JWT.require(algorithm).withIssuer("auth0").build().verify(token);
        return jwt.getClaims().get("login").asString();
    }
}
