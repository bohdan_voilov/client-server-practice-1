package util;

import lombok.SneakyThrows;
import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;

public class Hasher {

    @SneakyThrows
    public static String hash(String s){
        return DigestUtils
                .md5Hex(s).toUpperCase();
    }
}
